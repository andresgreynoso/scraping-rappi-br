package com.avalith.scrapingrappi.utilities;

public enum WebSiteEnum {
    IFOOD("ifood.com.co");

    public String domain;

    private WebSiteEnum(String domain) {
        this.domain = domain;
    }
}
