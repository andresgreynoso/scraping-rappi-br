package com.avalith.scrapingrappi.utilities;

public class Utilities {
    public static String jsonToStringUrl(String jsonUrl) {
        var segmentedUrl = jsonUrl.split(":");
        String url = "";
        if(jsonUrl.contains("http")) {
            url = segmentedUrl[1] + ":" + segmentedUrl[2];
        } else {
            url = segmentedUrl[1];
        }
        url = url.replace("\"", "");
        url = url.replace("\\n", "");
        url = url.replace("}","");
        return url;
    }

    public static String onlyDecimals() {
        return "[^.0-9]";
    }
}
