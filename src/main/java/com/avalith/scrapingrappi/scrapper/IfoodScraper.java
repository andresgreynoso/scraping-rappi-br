package com.avalith.scrapingrappi.scrapper;

import com.avalith.scrapingrappi.model.Corridor;
import com.avalith.scrapingrappi.model.MenuItem;
import com.avalith.scrapingrappi.utilities.Utilities;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class IfoodScraper extends MenuScraper {
    private final Document document;
    private final String sessionId;

    public IfoodScraper(String url) throws IOException {
         var connection = Jsoup.connect(url)
                .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36")
                .execute();
        sessionId = connection.cookie("SESSION");
        document = connection.parse();

    }

    @Override
    public List<Corridor> scrapeMenu() {
        var corridorElements = document.select("[id^=nav]");
        System.out.println();
        document.select()
        return IntStream.range(0, corridorElements.size())
                .mapToObj(corridorIndex -> getCorridor(corridorElements.get(corridorIndex), corridorIndex))
                .collect(Collectors.toList());
    }

    private Corridor getCorridor(Element corridorElement, int corridorIndex) {
        var corridor = new Corridor();

        // Corridor Index
        corridor.setIndex(corridorIndex);

        // Corridor name
        String corridorName = corridorElement.getElementsByAttribute("data-category").attr("data-category");
        corridor.setName(corridorName);

        // Products
        var productElements = corridorElement.getElementsByTag("li");
        var products = IntStream.range(0, productElements.size())
                .mapToObj(productIndex -> getProduct(productElements.get(productIndex), productIndex))
                .collect(Collectors.toList());

        return corridor;
    }

    private MenuItem getProduct(Element productElement, int productIndex) {
        var product = new MenuItem();

        // Product Index
        product.setIndex(productIndex);

        // Product Name
        System.out.println();
        String productName = productElement.getElementsByTag("h4").text();
        product.setName(productName);

        // Product Description
        String productDescription = productElement.getElementsByTag("p").text();
        product.setDescription(productDescription.isBlank() ? productName : productDescription);

        // Product Price
        String productPrice = productElement.getElementsByTag("span").first().text().replaceAll(Utilities.onlyDecimals(), "");
        product.setPrice();


        // Topping Group

        return null;
    }

}
