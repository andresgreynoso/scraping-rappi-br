package com.avalith.scrapingrappi.scrapper;

import com.avalith.scrapingrappi.model.Corridor;

import java.util.List;

public abstract class MenuScraper {

    public MenuScraper (){
    }

    public abstract List<Corridor> scrapeMenu();
}
