package com.avalith.scrapingrappi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScrapingRappiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScrapingRappiApplication.class, args);
	}

}
