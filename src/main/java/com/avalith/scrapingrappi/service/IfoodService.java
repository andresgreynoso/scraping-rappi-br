package com.avalith.scrapingrappi.service;

import com.avalith.scrapingrappi.model.Corridor;
import com.avalith.scrapingrappi.scrapper.IfoodScraper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class IfoodService {
    public List<Corridor> getMenuItemsFromRestaurant(final String url) throws IOException {
        return new IfoodScraper(url).scrapeMenu();
    }
}
