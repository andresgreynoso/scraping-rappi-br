package com.avalith.scrapingrappi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Corridor {
  private int index;
  private List<MenuItem> products;

  @JsonProperty("corridor_name")
  private String name;

  public Corridor() {
    products = new ArrayList<>();
  }

  public void addProduct(MenuItem product) {
    products.add(product);
  }
}
