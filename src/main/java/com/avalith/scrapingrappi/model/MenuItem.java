package com.avalith.scrapingrappi.model;

import com.avalith.scrapingrappi.dto.MenuItemDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class MenuItem {

  private int index;
  private String name;
  private String description;
  private Double price;
  private boolean active;
  private boolean available;

  @JsonProperty("topping_group")
  private List<ToppingCategory> toppingGroup;

  public MenuItem() {
    toppingGroup = new ArrayList<>();
    active = true;
    available = true;
  }

  public void addToppingCategory(ToppingCategory toppingCategory) {
    this.toppingGroup.add(toppingCategory);
  }

  public MenuItemDTO toDTO() {
    return new MenuItemDTO(this);
  }
}
