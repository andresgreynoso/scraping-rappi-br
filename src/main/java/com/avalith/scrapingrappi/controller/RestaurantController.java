package com.avalith.scrapingrappi.controller;

import com.avalith.scrapingrappi.service.*;
import com.avalith.scrapingrappi.utilities.Utilities;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

import static com.avalith.scrapingrappi.utilities.WebSiteEnum.*;

@RestController
@RequestMapping("/restaurant")
public class RestaurantController {

  private final IfoodService ifoodService;

  public RestaurantController(IfoodService ifoodService) {

    this.ifoodService = ifoodService;
  }

  @PostMapping
  public ResponseEntity getMenu(
          @RequestBody String url) throws IOException {
    String convertedUrl = Utilities.jsonToStringUrl(url);
    ResponseEntity responseEntity = ResponseEntity.badRequest().build();
   if(convertedUrl.contains(IFOOD.domain))
      responseEntity = new ResponseEntity(ifoodService.getMenuItemsFromRestaurant(convertedUrl), HttpStatus.OK);

    return responseEntity;
  }
}
